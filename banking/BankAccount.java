package banking;

import java.util.Scanner;

public class BankAccount {
    float balance;
    float previousTransaction;
    String customerName;
    String customerId;

    public BankAccount(String cname, String cid) {
        customerName = cname;
        customerId = cid;
    }

    void deposit(float amount) {
        if (amount > 0) {
            balance = balance + amount;
            previousTransaction = amount;
        }
    }

    void withdraw(float amount) {
        if (amount > 0) {
            balance = balance - amount;
            previousTransaction = -amount;
        }
    }

    void getPreviousTransaction() {
        if (previousTransaction > 0) {
            System.out.println("Deposited: " + previousTransaction);  
        } else if (previousTransaction < 0) {
            System.out.println("Withdrawn: " + Math.abs(previousTransaction));  
        } else {
            System.out.println("No transaction occured");  
        }
    }

    public void showMenu() {
        char option = '\0';
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome " + customerName);
        System.out.println("Your ID is " + customerId);
        System.out.println("\n");
        System.out.println("A. Check Balance");
        System.out.println("B. Deposit");
        System.out.println("C. Withdraw");
        System.out.println("D. Previous Transaction");
        System.out.println("E. Exit");

        do {
            System.out.println("-----------------------------------");
            System.out.println("Enter an option");
            System.out.println("-----------------------------------");
            option = scanner.next().charAt(0);
            System.out.println("\n");

            switch (option) {
                case 'A':
                    System.out.println("-----------------------------------");
                    System.out.println("Balance = " + balance);
                    System.out.println("-----------------------------------");
                    System.out.println("\n");
                    break;

                case 'B':
                    System.out.println("-----------------------------------");
                    System.out.println("Enter an amount to be deposited");
                    System.out.println("-----------------------------------");
                    float depositAmount = scanner.nextFloat();
                    deposit(depositAmount);
                    System.out.println("\n");
                    break;

                case 'C':
                    System.out.println("-----------------------------------");
                    System.out.println("Enter an amount to be withdrawn");
                    System.out.println("-----------------------------------");
                    float withdrawalAmount = scanner.nextFloat();
                    withdraw(withdrawalAmount);
                    System.out.println("\n");
                    break;

                case 'D':
                    System.out.println("-----------------------------------");
                    getPreviousTransaction();
                    System.out.println("-----------------------------------");
                    System.out.println("\n");
                    break;

                case 'E':
                    System.out.println("************************************");
                    break;
            
                default:
                    System.out.println("Invalid Option!!" + "\n" + "Please enter again");
                    break;
            }
        } while (option != 'E');

        System.out.println("Thank you for using our services");
    }
}