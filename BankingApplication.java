import banking.BankAccount;

public class BankingApplication {
    public static void main(String[] args) {
        BankAccount account = new BankAccount("Some Dude", "BA001");
        account.showMenu();
    }
}